package io.springSecurity.demo.controller;

import io.springSecurity.demo.model.AuthenticationRequest;
import io.springSecurity.demo.model.AuthenticationResponse;
import io.springSecurity.demo.service.MyUserDetailService;
import io.springSecurity.demo.util.JwtUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Log4j
@RestController
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private MyUserDetailService myUserDetailService;
    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("/")
    public String allAllow(){
        log.info("Allow any roles");
        return ("<h1> Allow any roles </h1>");
    }

    @GetMapping("/user")
    public String userAllow(){
        log.info("Allow users and admins");
        return ("<h1> Allow users and admins </h1>");
    }

    @GetMapping("/admin")
    public String adminAllow(){
        log.info("Just admin can access");
        return ("<h1> Just admin can access </h1>");
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest request) throws Exception{
        try{
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getUsername(),request.getPassword())
            );
        }catch (BadCredentialsException e) {
            log.error("Bad credentials! "+request.getUsername());
        }

        final UserDetails userDetails = myUserDetailService.loadUserByUsername(request.getUsername());

        final String jwtToken = jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwtToken));
    }
}
